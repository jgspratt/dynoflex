# HelloWorld

This example program will initiate communication with up to three ports.
The port number is automaticaly detected when using the USB connection on the SC4-Hub board.

As of 2019-12-28, this project (`HelloWorld.sln`) actually compiles on Windows, so that says that the project has roughly the right setup.
The other `.sln` files in this repository are not working on Windows, so, future self, don't bother with them.
