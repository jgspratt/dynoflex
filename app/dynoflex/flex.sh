#!/usr/bin/env bash

# Usage: See README.md

rm -f dynoflex.exe

printf "Compiling..."
g++ -std=c++14 dynoflex.cpp -o dynoflex.exe
printf "done!\n\n"

printf "START dynoflex.exe...\n\n"

# ./dynoflex.exe com_port_num resolution_counts <move1> <move2>...

./dynoflex.exe \
  "0" \
  "10000" \
  "200" "5" "100" "1.5"  "50000" "1" \
  "200" "5" "100" "1.5" "-50000" "1" \
  "200" "5" "100" "1.5"  "50000" "1" \
  "200" "5" "100" "1.5" "-50000" "1" \
  "200" "5" "100" "1.5"  "50000" "1" \
  "200" "5" "100" "1.5" "-50000" "1" \
  "200" "5" "100" "1.5"  "50000" "1" \
  "200" "5" "100" "1.5" "-50000" "1" \
  "200" "5" "100" "1.5"  "50000" "1" \
  "200" "5" "100" "1.5" "-50000" "1"


#./dynoflex.exe \
#  max_torque max_accel accel delta \
#  max_torque2 max_accel2 accel2 delta2 \

printf "\n...DONE!\n"

