# Usage: See README.md

$COM_PORT             = 4  # See device manager
$RESOLUTION_COUNTS    = 10000

$ACC                  = 200  # Acceleration (RPM/s)
$TRQ                  = 100  # Torque (%)
$RPM                  = 120  # Max speed (RPM)

$DO_LOG               = 1  # Yes, log
$NO_LOG               = 0  # No, do not log
$LOG_DIR              = 'log/tas'

$PRE_TRQ_PCT          = 0.0  # Torque before a pre-move (usually zero)
$START_TRQ_PCT        = 2.0  # Torque required to start a move
$CONT_TRQ_PCT         = 2.0  # Torque required to continue a move

## Pull Down
$PULL_DOWN_PRE        = 550000
$PULL_DOWN_COUNT      = 400000

$pre_home = 1

write-host @"




_ _ _ ____ _    ____ ____ _  _ ____    ___  ____ ____ _  _      ____ ____ _  _ _  _ ____ _  _ ___  ____ ____
| | | |___ |    |    |  | |\/| |___    |__] |__| |    |_/       |    |  | |\/| |\/| |__| |\ | |  \ |___ |__/
|_|_| |___ |___ |___ |__| |  | |___    |__] |  | |___ | \_ .    |___ |__| |  | |  | |  | | \| |__/ |___ |  \
                                                           '





"@
write-host ""
write-host ""
write-host ""
write-host ""
write-host "START dynoflex.exe..."
write-host ""


## PULL DOWN
############
$confirm = read-host "THEDE: READY FOR ***PULL DOWN*** (NARROW, PALM IN) (PEG 4)? (y/n)"
if ($confirm -eq "y") {
  write-host "PULL DOWN - START"
  write-host ""

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/pull-down/$(get-date -format 'yyyy-MM-dd_HHmmss')_pull-down.txt" `
    "$ACC" "5"    "$RPM" "$PRE_TRQ_PCT"     "$PULL_DOWN_PRE"   "$NO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT"   "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"    "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"    "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"    "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"    "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "-$PULL_DOWN_COUNT" "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "PULL DOWN - DONE"
  write-host ""
  write-host ""
  write-host ""
}

write-host "ALL DONE!"
write-host "Remember to upload your logs via 'git commit...'."
write-host ""
write-host ""
write-host ""
write-host ""
write-host ""
