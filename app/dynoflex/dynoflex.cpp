#include <chrono>
#include <iostream>
#include <fstream>
#include <iterator>
#include <list>
#include <sstream>
#include <stdio.h>
#include <string>
#include <thread>
#include <unordered_map>
#include<algorithm>

// <sFnd>
// /*
#include "pubSysCls.h"
using namespace sFnd;
//#define CHANGE_NUMBER_SPACE   2000  // TODO: not used?
// */
// </sFnd>

const int NODE_NUM = 0;                   // Just one node: node 0
const int TIME_TILL_TIMEOUT_MS = 20000;   // The timeout used for homing(ms) (homing is done really slowly)
const int MOVE_GRACE_TIMEOUT_MS = 2500;   // The extra grace period for completing a move (beyond the estimate)
const double NEGATIVE_FRICTION_COMP_PERCENT = 2.26;  // How much harder the machine (fighting friction) has to work to pull up versus let down at 100lbs
const double POSITIVE_FRICTION_COMP_PERCENT = -0.286;  // How much harder the person (fighting friction) has to work to pull down versus let up at 100lbs
const double LBS_PER_TORQUE_PERCENT = 18.9;  // Hanging at neutral measured with 100lbs
int com_port_num;
int resolution_counts;

// Send message and wait for newline
void msgUser(const char *msg) {
  printf("%s", msg);
  getchar();
}

// <debug>
void print_list(std::list <std::string> g) {
  std::cout << "\nprint_list():\n";
  for (std::list<std::string>::const_iterator i = g.begin(); i != g.end(); ++i) {
    std::cout << " " << i->c_str();
  }
  std::cout << '\n';
}
// </debug>

// <debug>
void print_list_of_moves(std::list <std::unordered_map<std::string, std::string>> list_of_moves) {
  std::cout << "\nprint_list_of_moves():\n";
  for (auto& this_map : list_of_moves) {
    std::cout << "\n\nMOVE:\n";
    std::cout << "- max_acc:         " << this_map["max_acc"] << '\n';
    std::cout << "- max_torque:      " << this_map["max_torque"] << '\n';
    std::cout << "- max_vel:         " << this_map["max_vel"] << '\n';
    std::cout << "- torque_to_start: " << this_map["torque_to_start"] << '\n';
    std::cout << "- delta:           " << this_map["delta"] << '\n';
  }
  std::cout << '\n';
}
// </debug>

int execute_list_of_moves(std::list <std::unordered_map<std::string, std::string>> list_of_moves, int com_port_num, bool do_homing, std::string log_filepath) {
  std::ofstream log;
  log.open(log_filepath);
  std::string com_port_num_str = std::to_string(com_port_num);
  printf("COM Port Number: %d\n\n", com_port_num);

  //msgUser("Press Enter to connect to the Dynoflex.");

  // <sFnd>
// /*
  printf("Define comHubPorts... "); // debug
  std::vector<std::string> comHubPorts;
  printf("done!\n"); // debug


  // Create the SysManager object. This object will coordinate actions among various ports
  // and within nodes. In this example we use this object to setup and open our port.
  printf("Create the SysManager object..."); // debug
  SysManager* sys_manager = SysManager::Instance();  // Create System Manager sys_manager
  std::cout << "initial sys_manager->TimeStampMsec(): " << std::to_string(sys_manager->TimeStampMsec()) << "\n";
  printf("done!\n\n"); // debug


  // This will try to open the port. If there is an error/exception during the port opening,
  // the code will jump to the catch loop where detailed information regarding the error will be displayed;
  // otherwise the catch loop is skipped over
  printf("FindComHubPorts - START\n"); // debug
  SysManager::FindComHubPorts(comHubPorts);
  int port_count = comHubPorts.size();
  printf("Found %d SC Hubs (comHubPorts.size()=1)\n", port_count);
  printf("FindComHubPorts - DONE\n\n"); // debug

  // Up to this point, the code runs fine on Win without a hub connected.


//  /*
  try {
    printf("sys_manager->ComHubPort(%d, %d) - START\n", NODE_NUM, com_port_num); // debug
    // Define the first SC Hub port (port 0) to be associated with com_port_num (as seen in device manager)
    sys_manager->ComHubPort(NODE_NUM, com_port_num);  // At this point, it seems you need to be connected to the COM port device
    printf("sys_manager->ComHubPort() - DONE\n\n"); // debug

    printf("sys_manager->PortsOpen() - START\n");
    sys_manager->PortsOpen(1);  // Open the first port.
    printf("sys_manager->PortsOpen() - DONE\n\n");
  } catch (mnErr theErr) {   // This catch statement will intercept any error from the Class library
    printf("Port Failed to open, shutting down\n");
    //This statement will print the address of the error, the error code (defined by the mnErr class),
    //as well as the corresponding error message.
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr, theErr.ErrorCode, theErr.ErrorMsg);

    ::system("pause"); //pause so the user can see the error message; waits for user to press a key

    return -1;  //This terminates the main program
  }

  printf("IPort &this_port - START\n");
  IPort &this_port = sys_manager->Ports(0);
  printf("IPort &this_port - DONE\n\n");

  printf("COM Port[%d]: port_state=%d, node_count=%d\n", this_port.NetNumber(), this_port.OpenState(), this_port.NodeCount());

  // Once the code gets past this point, it can be assumed that the Port has been opened without issue
  // Now we can get a reference to our port object which we will use to access the node objects
  INode &this_node = this_port.Nodes(NODE_NUM);

  // <debug>
  printf("   Node[%d]: type=%d\n", 0, this_node.Info.NodeType());
  printf("            userID: %s\n", this_node.Info.UserID.Value());
  printf("        FW version: %s\n", this_node.Info.FirmwareVersion.Value());
  printf("          Serial #: %d\n", this_node.Info.SerialNumber.Value());
  printf("             Model: %s\n", this_node.Info.Model.Value());
  // </debug>

  // <debug>
  printf("Node \t%d enabled\n\n", NODE_NUM);
  // </debug>

  // TODO: do we actually need/want this code to enable the node?
  // Define a timeout in case the node is unable to enable
  // This will loop checking on the Real time values of the node's Ready status
  double timeout = sys_manager->TimeStampMsec() + TIME_TILL_TIMEOUT_MS;
  while (!this_node.Motion.IsReady()) {  // while node is not enabled
    if (sys_manager->TimeStampMsec() > timeout) {
      printf("Error: Timed out waiting for Node %d to enable\n", NODE_NUM);
      msgUser("Press Enter to exit."); // Pause so the user can see the error message; waits for user to press a key
      return -2;
    }
  }

  if (do_homing) {
    printf("\n\n");
    printf("*** HOMING ***\n");
    msgUser("Press any key to HOME.  DO NOT TOUCH MACHINE UNTIL HOMING COMPLETE.");
    printf("Homing - START\n");
    this_node.Motion.Homing.Initiate();
    timeout = sys_manager->TimeStampMsec() + TIME_TILL_TIMEOUT_MS;
    while (!this_node.Motion.Homing.WasHomed()) {
      if (sys_manager->TimeStampMsec() > timeout) {
        printf("Node did not complete homing:  \n\t -Ensure Homing settings have been defined through ClearView. \n\t -Check for alerts/Shutdowns \n\t -Ensure timeout is longer than the longest possible homing move.\n");
        msgUser("Press Enter to exit."); // pause so the user can see the error message; waits for user to press a key
        return -2;
      }
    }
    printf("Homing - DONE\n\n");
   }

  // Set Units (all moves use the same units)
  this_node.AccUnit(INode::RPM_PER_SEC);          // Set the units for Acceleration to RPM/SEC
  this_node.VelUnit(INode::RPM);                  // Set the units for Velocity to RPM
  this_node.TrqUnit(this_node.PCT_MAX);           // Set the units for Torque to Percent of max

  printf("*** EXERCISE ***\n");
  int moves_in_set = list_of_moves.size();
  msgUser("Press Enter to begin this set.");

  std::ostringstream header_line;
  header_line << "rep_num\tposition\ttorque\ttorque_compensated\tlbs_compensated\n";
  std::string header_line_str = header_line.str();
  log << header_line_str;
  std::cout << "\n\n" << header_line_str << "\n";

  int move_num = 1;
  int rep_num = 1;
  for (auto& this_map : list_of_moves) {
    double max_acc          = std::stod(this_map["max_acc"]);
    double max_torque       = std::stod(this_map["max_torque"]);
    double max_vel          = std::stod(this_map["max_vel"]);
    double torque_to_start  = std::stod(this_map["torque_to_start"]);
    int delta               = std::stoi(this_map["delta"]);
    bool log_this;
    std::istringstream(this_map["log_this"]) >> log_this;
    bool is_rep = log_this;  // TODO: this assumes every logged motion is a rep

    int fake_sleep = std::abs(std::lround(delta / 800));

    // <debug>
    double estimated_time = this_node.Motion.MovePosnDurationMsec(delta);
    printf("\n\nRep %d:\n", rep_num);
    printf("- max_acc:         %0.0f RPM/s\n", max_acc);
    printf("- max_torque:      %0.0f %%\n", max_torque);
    printf("- TrqGlobal:       %f \n", this_node.Limits.TrqGlobal.Value());
    printf("- max_vel:         %0.0f RPM\n", max_vel);
    printf("- torque_to_start: %0.2f %%\n", torque_to_start);
    printf("- delta:           %d counts\n", delta);
    printf("- log_this:        %s \n", log_this ? "true" : "false");
    printf("- time:            %.1fs\n\n", (estimated_time/1000));
    // </debug>

    this_node.Limits.TrqGlobal = max_torque;        // Set Torque Limit (%)
    this_node.Motion.AccLimit = max_acc;            // Set Acceleration Limit (RPM/Sec)
    this_node.Motion.VelLimit = max_vel;            // Set Velocity Limit (RPM)
    this_node.Motion.TrqMeasured.AutoRefresh(true); // Fetch the current torque

    double this_torque = std::abs(static_cast<double>(this_node.Motion.TrqMeasured));
    while (this_torque < torque_to_start) {
      printf("Waiting for actual torque (currently %.1f) to exceed %.1f...\n", this_torque, torque_to_start);
      std::this_thread::sleep_for(std::chrono::milliseconds(500));
      this_torque = std::abs(static_cast<double>(this_node.Motion.TrqMeasured));
    }

    std::list <int> list_of_measurement_times_ms;
    std::list <int> list_of_measurement_timestamps_ms;
    bool going_forward = (delta > 0) ? true : false;  // you're either going forward or you're going backward
    int singed_resolution_step_counts = going_forward ? resolution_counts : -1*resolution_counts;
    double friction_compensation = going_forward ? POSITIVE_FRICTION_COMP_PERCENT : NEGATIVE_FRICTION_COMP_PERCENT;
    int this_step_counts = 0;
    double total_prior_sleep_ms = 0;
    double this_step_sleep_ms = 0;
    double this_step_delta_sleep_ms = 0;
    while (std::abs(this_step_counts) <= std::abs(delta)) {
      //this_step_sleep_ms = std::abs(std::lround(this_step_counts / 100));  // <stub>
      this_step_sleep_ms = this_node.Motion.MovePosnDurationMsec(this_step_counts);
      this_step_delta_sleep_ms = this_step_sleep_ms - total_prior_sleep_ms;
      //list_of_measurement_times_ms.push_back((int)(this_step_delta_sleep_ms));
      list_of_measurement_timestamps_ms.push_back((int)(this_step_sleep_ms));
      total_prior_sleep_ms = this_step_sleep_ms;
      this_step_counts = this_step_counts + singed_resolution_step_counts;
    }

    printf("START MOVE\n");
    this_node.Motion.MovePosnStart(delta);      // Actually execute the move
    double move_start_timestamp = sys_manager->TimeStampMsec();
    printf("MOVE STARTED\n\n");
    // Define a timeout in case the node is unable to enable to complete the move
    double timeout = move_start_timestamp + estimated_time + MOVE_GRACE_TIMEOUT_MS;

    int measurement_count = 0;
    double this_torque_friction_compensated;
    double this_lbs_compensated;
    for (auto& this_measurement_timestamp : list_of_measurement_timestamps_ms) {
      while (sys_manager->TimeStampMsec() < (move_start_timestamp + this_measurement_timestamp)) {
        std::this_thread::sleep_for(std::chrono::milliseconds(10));
      }
      //printf("Take an average of X measurements for smoothing\n");
      int sample = 1;
      int samples = 20;  // One sample costs about 2.5 ms
      //int sample_resolution_ms = 5;
      this_torque = 0;
      //double timestamp_before_samples = sys_manager->TimeStampMsec();
      while (sample <= samples) {
        this_torque += std::abs(static_cast<double>(this_node.Motion.TrqMeasured));
        sample++;
      }
      //double timestamp_after_samples = sys_manager->TimeStampMsec();
      //printf("timestamp_before_samples: %f\ttimestamp_after_samples: %f\tsamples took: %f ms\n", timestamp_before_samples, timestamp_after_samples, (timestamp_before_samples-timestamp_after_samples));
      this_torque /= samples;


      this_torque_friction_compensated = std::max(
        (
          this_torque - (
            std::min(
              (LBS_PER_TORQUE_PERCENT*this_torque/100.0)*friction_compensation,
              friction_compensation
            )
          )
        ),
        0.0
      );  // TODO: I am just guessing at how to scale this; for now, scale friction to 100lbs and stop there.
      this_lbs_compensated = this_torque_friction_compensated*LBS_PER_TORQUE_PERCENT;

      std::ostringstream log_line;
      log_line.precision(3);
      log_line << rep_num;
      log_line << "\t";
      log_line << measurement_count*singed_resolution_step_counts;
      log_line << "\t";
      log_line << this_torque;
      log_line << "\t";
      log_line << this_torque_friction_compensated;
      log_line << "\t";
      log_line << this_lbs_compensated;
      log_line << "\n";
      log_line << std::flush;
      std::string log_line_str = log_line.str();

      std::cout << log_line_str;
      if (log_this) {
        log << log_line_str;
      }

      if (sys_manager->TimeStampMsec() > timeout) {
        printf("Error: Timed out waiting for move to complete\n");
        msgUser("Press any key to continue."); // pause so the user can see the error message; waits for user to press a key
        return -2;
      }
      measurement_count = measurement_count+1;
    }
    printf("\nMove Done\n");
    move_num++;
    if (is_rep && going_forward) {
      rep_num++;
    }
  }
  std::cout << '\n';
  sys_manager->PortsClose();
  log.close();
  return 0;
}


std::list <std::unordered_map<std::string, std::string>> get_list_of_moves(int argc, char *argv[]) {
  std::list <std::unordered_map<std::string, std::string>> list_of_moves;
  std::unordered_map<std::string, std::string> this_move;
  for (int move_param_num = 0; move_param_num < argc; ++move_param_num) {
    int move_param_num_mod_6 = move_param_num % 6;  // 6 parameters per move
    std::string move_param_num_mod_6_str = std::to_string(move_param_num_mod_6);

    switch (move_param_num_mod_6) {
      case 0:
        this_move["max_acc"] = argv[move_param_num];
        break;
      case 1:
        this_move["max_torque"] = argv[move_param_num];
        break;
      case 2:
        this_move["max_vel"] = argv[move_param_num];
        break;
      case 3:
        this_move["torque_to_start"] = argv[move_param_num];
        break;
      case 4:
        this_move["delta"] = argv[move_param_num];
        break;
      case 5:
        this_move["log_this"] = argv[move_param_num];
        list_of_moves.push_back(this_move);
        std::unordered_map<std::string, std::string> this_move;
        break;
    }
  }
  // <debug>
//  print_list_of_moves(list_of_moves);
  // </debug>

  return list_of_moves;
}


int main(int argc, char *argv[]) {
  printf("Begin Dynoflex...\n");

  std::string program_name = argv[0];
  com_port_num = atoi(argv[1]);
  resolution_counts = atoi(argv[2]);
  bool do_homing;
  std::istringstream(argv[3]) >> do_homing;
  std::string log_filepath = argv[4];
  std::cout << "Log file path: " << log_filepath << "\n";
  int non_move_args_count = 5;

  std::list <std::unordered_map<std::string, std::string>> list_of_moves = get_list_of_moves(argc - non_move_args_count, argv + non_move_args_count);

  // <debug>
//  print_list_of_moves(list_of_moves);
  // </debug>
  int ret = execute_list_of_moves(list_of_moves, com_port_num, do_homing, log_filepath);
  // <debug>
  printf("End Dynoflex!\n");
  //msgUser("Press Enter to complete set.");
  // </debug>
  return 0;
}
