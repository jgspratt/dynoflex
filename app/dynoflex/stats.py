#!/usr/bin/env python3

# Usage:
#
# > python .\stats.py --log-dir jgs

import argparse
import os
import pandas  # pip3 install pandas
import sys
import pathlib

SEP = '\t'

parser = argparse.ArgumentParser(
    description="Calculate stats for a dynoflex log."
)
parser.add_argument(
  '--log-dir',
  nargs='?',
  type=str,
  const='jgs',
  help="Log dir on which to calculate stats (e.g., 'jgs' for ./log/jgs/*)",
  dest="log_dir",
)

args, unparsed = parser.parse_known_args()
log_dir = args.log_dir
print(f'{log_dir=}')

exercises = pathlib.Path(f'log/{log_dir}').glob('*')

for exercise in exercises:
  sets = pathlib.Path(exercise).glob('*.txt')
  sets_sorted = sorted(sets)
  print(f'\n{os.path.basename(exercise)}:')
  print(f'date\ttime\tmean\tmed\tpct75\tpct90\t+mean\t-mean\t+max\t-max\tr1\tr2\tr3\tr4\tr5')
  for this_set in sets_sorted:
    date    = os.path.basename((this_set))[0:10]
    time    = os.path.basename((this_set))[11:15]
    log     = pandas.read_csv(this_set, sep=SEP)
    mean    = round(log.lbs_compensated.mean(), 0)
    median  = round(log.lbs_compensated.quantile(0.5),0)
    pct75   = round(log.lbs_compensated.quantile(0.75), 0)
    pct90   = round(log.lbs_compensated.quantile(0.90), 0)
    r1      = round(log[log.rep_num == 1].lbs_compensated.mean(), 0)
    r2      = round(log[log.rep_num == 2].lbs_compensated.mean(), 0)
    r3      = round(log[log.rep_num == 3].lbs_compensated.mean(), 0)
    r4      = round(log[log.rep_num == 4].lbs_compensated.mean(), 0)
    r5      = round(log[log.rep_num == 5].lbs_compensated.mean(), 0)
    p_mean  = round(log[log.position > 0].lbs_compensated.mean(), 0)
    p_max   = round(log[log.position > 0].lbs_compensated.max(), 0)
    n_mean  = round(log[log.position < 0].lbs_compensated.mean(), 0)
    n_max   = round(log[log.position < 0].lbs_compensated.max(), 0)
    print(f'{date}\t{time}\t{mean:.0f}\t{median:.0f}\t{pct75:.0f}\t{pct90:.0f}\t{p_mean:.0f}\t{n_mean:.0f}\t{p_max:.0f}\t{n_max:.0f}\t{r1:.0f}\t{r2:.0f}\t{r3:.0f}\t{r4:.0f}\t{r5:.0f}')

#
# log = pandas.read_csv(log_file, sep='\t')
#
# positive_mean = log[log.position > 0].lbs_compensated.mean()
# positive_max = log[log.position > 0].lbs_compensated.max()
# negative_mean = log[log.position < 0].lbs_compensated.mean()
# negative_max = log[log.position < 0].lbs_compensated.max()
# print(f'positive_mean={round(positive_mean, 1)}, positive_max={round(positive_max, 1)}, negative_mean={round(negative_mean, 1)}, negative_max={round(negative_max, 1)}')
#
