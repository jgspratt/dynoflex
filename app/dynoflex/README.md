# Dynoflex Usage

Check out `flex.sh` for how I'm calling the exe currently.
As of now, it does not do anything.

But it will.

## Proposed usage

```
./dynoflex.exe \
  <com_port_num> \
  <resolution_counts> \
  <home?> \
  <logFilePath> \
  <move_1> \
  [<move_2> [<move_N>]]

<resolution_counts>   ::= How many counts between torque measurements (10000 is a good number)
<com_port_num>        ::= COM port number as seen in Device Manager (the first node on this port will be used) (e.g., "4")
<do_homing>           ::= Should homing happen before the move? (e.g., "1" for "yes"; "0" for "no")

<move>                ::= <max_acc> <max_torque> <max_vel> <torque_to_start> <delta> <record?>

Parameter                 Format    Units     Example   Meaning
<max_acc>             ::= (double)  (RPM/s)   400       Acceleration limit - the fastest the motor will accelerate
<max_torque>          ::= (double)  %         100       Torque limit - the hardest the motor will twist
<max_vel>             ::= (double)  RPM       200       Velocity limit - the fastest the motor will move
<torque_to_start>     ::= (double)  %         1.5       Torque required to start the move - the motor will not move until this is seen (not implemented yet)
<delta>               ::= (int)     Counts    180000    How far to move in one direction (example: use -180000 to return to the start)
<log_this>            ::= (int)     1/0       1         Should this move be recorded (1 for "yes"; 0 for "no") - useful for "pre"/"post" moves (setup or warmup)


Example (5 reps @ 471,000 counts @ 100 RPM @ 200 RPM/s accel @ 100% torque max on port 4 with pre-home, recording every 10000 counts, requiring 2% of torque to start moving):

./dynoflex.exe \
  4 \
  10000 \
  1 \
  200 100 100 2  471000 1 \
  200 100 100 2 -471000 1 \
  200 100 100 2  471000 1 \
  200 100 100 2 -471000 1 \
  200 100 100 2  471000 1 \
  200 100 100 2 -471000 1 \
  200 100 100 2  471000 1 \
  200 100 100 2 -471000 1 \
  200 100 100 2  471000 1 \
  200 100 100 2 -471000 1
```


## Perform actions

* Perform an actual move: `theNode.Motion.MovePosnStart(MOVE_DISTANCE_CNTS);`
* Estimate time to complete a move: `printf("%f estimated time.\n", theNode.Motion.MovePosnDurationMsec(MOVE_DISTANCE_CNTS));`


## Measure things

* Measure actual torque: `double myTrq = myNode.Motion.TrqMeasured;`
    * The default units are `PCT_MAX`
    * Can also use `AMPS`
* Measure the actual position: `double myPosn = myNode.Motion.PosnMeasured;`
* Measure the actual velocity: `double myVel = myNode.Motion.VelMeasured;` (`RPM` by default; `COUNTS_PER_SEC` also available)


## Set Units

* Acceleration: `theNode.AccUnit(INode::RPM_PER_SEC);`
* Velocity: `theNode.VelUnit(INode::RPM);`
* Torque:
  * Percent: `myNode.TrqUnit(myNode.PCT_MAX);`
  * Amps: `myNode.TrqUnit(mynode.AMPS);`


## Set Limits

* Max Torque (global): `myNode.Limits.TrqGlobal = 100;`
* Max Acceleration: `theNode.Motion.AccLimit = ACC_LIM_RPM_PER_SEC;`
* Max Velocity: `theNode.Motion.VelLimit = VEL_LIM_RPM`
    * `double velLim = myNode.Motion.VelLimit; `


## Other Notes

`virtual void sFnd::IHoming::SignalComplete()`

"Calling this function will cause soft limits to activate."

I'm guessing those come from the clearpath GUI setup procedure


```c++
// Set the torque Limit to 100%
myNode.TrqUnit(myNode.PCT_MAX);
myNode.Limits.TrqGlobal = 100;


// Print out the torque limit
cout << myNode.Limits.TrqGlobal.Value() << endl;
// Alternate access way
double myTrqLimit = myNode.Limits.TrqGlobal;


LIM_ACC_RPM_PER_SEC       sFnd::IMotion::AccLimit
LIM_VEL_RPM               sFnd::IMotion::VelLimit
LIM_TIMEOUT_HOMING

ACC_LIM_RPM_PER_SEC   10
VEL_LIM_RPM           15
MOVE_DISTANCE_CNTS    32000
NUM_MOVES             5
TIME_TILL_TIMEOUT     10000  //The timeout used for homing(ms)
CHANGE_NUMBER_SPACE   2000



theNode.Motion.Homing.Initiate();


sFnd::IHoming::IsHoming
!theNode.Motion.Homing.WasHomed()


theNode.Motion.MovePosnStart(
  int32_t target,
  bool targetIsAbsolute=false,
  bool addPostMoveDwell=false,
)


Parameters

[in] target
  Target position. This is either the absolute position of relative to the current position depending on the targetIsAbsolute argument.

[in] targetIsAbsolute
  the target is an absolute destination.

[in] addPostMoveDwell
  delay the next move being issued by DwellMs after this move profile completes.

Returns
Number of additional moves the node will accept.

The move is kinematically limited by the current acceleration, and velocity limits.

Move requests are buffered and requests initiated during an active move will execute immediately after the active move completes. The return value will tell you the number of additional moves that maybe queued when this request if processed. You can also use the Status Register's _cpmStatusRegFlds::MoveBufAvail field, which will be deasserted when the buffer is full.




Also take a look at sFnd::IMotionAdv for Asymetric moves, etc.





      for (size_t i = 0; i < NUM_MOVES; i++) {  // for each move
        for (size_t iNode = 0; iNode < myPort.NodeCount(); iNode++) {  // for each node (on this port)
          // Create a shortcut reference for a node
          INode &theNode = myPort.Nodes(iNode);

          theNode.Motion.MoveWentDone();                      // Clear the rising edge Move done register

          theNode.AccUnit(INode::RPM_PER_SEC);                // Set the units for Acceleration to RPM/SEC
          theNode.VelUnit(INode::RPM);                        // Set the units for Velocity to RPM
          theNode.Motion.AccLimit = ACC_LIM_RPM_PER_SEC;      // Set Acceleration Limit (RPM/Sec)
          theNode.Motion.VelLimit = VEL_LIM_RPM;              // Set Velocity Limit (RPM)

          printf("Moving Node \t%zi \n", iNode);
          theNode.Motion.MovePosnStart(MOVE_DISTANCE_CNTS);   // Execute 10000 encoder count move
          printf("%f estimated time.\n", theNode.Motion.MovePosnDurationMsec(MOVE_DISTANCE_CNTS));

          // Define a timeout in case the node is unable to enable
          double timeout = myMgr->TimeStampMsec() + theNode.Motion.MovePosnDurationMsec(MOVE_DISTANCE_CNTS) + 100;

          while (!theNode.Motion.MoveIsDone()) {
            if (myMgr->TimeStampMsec() > timeout) {
              printf("Error: Timed out waiting for move to complete\n");
              msgUser("Press any key to continue."); // pause so the user can see the error message; waits for user to press a key
              return -2;
            }
          }
          printf("Node \t%zi Move Done\n", iNode);
        } // for each node (on this port)
      } // for each move








```
