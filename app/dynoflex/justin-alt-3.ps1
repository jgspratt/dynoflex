# Usage: See README.md

$COM_PORT             = 4  # See device manager
$RESOLUTION_COUNTS    = 10000

$ACC                  = 200  # Acceleration (RPM/s)
$TRQ                  = 100  # Torque (%)
$RPM                  = 120  # Max speed (RPM)

$DO_LOG               = 1  # Yes, log
$NO_LOG               = 0  # No, do not log
$LOG_DIR              = 'log/jgs'

$PRE_TRQ_PCT          = 0.0  # Torque before a pre-move (usually zero)
$START_TRQ_PCT        = 4.0  # Torque required to start a move
$CONT_TRQ_PCT         = 2.0  # Torque required to continue a move

## Row
$ROW_PRE_POST         = 250000
$ROW_COUNT            = 450000

## Dip
$DIP_PRE_POST         = 715000
$DIP_COUNT            = 265000

## Dead Lift
$DEAD_LIFT_COUNT      = 400000

$pre_home_done = $False
$pre_home = 1

write-host @"




_ _ _ ____ _    ____ ____ _  _ ____    ___  ____ ____ _  _      ____ ____ _  _ _  _ ____ _  _ ___  ____ ____
| | | |___ |    |    |  | |\/| |___    |__] |__| |    |_/       |    |  | |\/| |\/| |__| |\ | |  \ |___ |__/
|_|_| |___ |___ |___ |__| |  | |___    |__] |  | |___ | \_ .    |___ |__| |  | |  | |  | | \| |__/ |___ |  \
                                                           '





"@
write-host ""
write-host ""
write-host ""
write-host ""
write-host "START dynoflex.exe..."
write-host ""


## ROW
######
$confirm = read-host "READY FOR ***ROW***? (y/n)"
if ($confirm -eq "y") {
  write-host "ROW - START"
  write-host ""
  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/row/$(get-date -format 'yyyy-MM-dd_HHmmss')_row.txt" `
    "$ACC" "5"    "$RPM" "$PRE_TRQ_PCT"   "$ROW_PRE_POST" "$NO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT" "$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$ROW_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$ROW_COUNT" "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "ROW - DONE"
  write-host ""
  write-host ""
  write-host ""
}

## DIP
######
$confirm = read-host "READY FOR ***DIP***? (y/n)"
if ($confirm -eq "y") {
  write-host "DIP - START"
  write-host ""
  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/dip/$(get-date -format 'yyyy-MM-dd_HHmmss')_dip.txt" `
    "$ACC" "5"    "$RPM" "$PRE_TRQ_PCT"   "$DIP_PRE_POST"  "$NO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT" "$DIP_COUNT"     "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DIP_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DIP_COUNT"    "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "DIP - DONE"
  write-host ""
  write-host ""
  write-host ""
}

# DEAD LIFT
##########
$confirm = read-host "READY FOR ***DEAD LIFT*** (SEAT REMOVED)? (y/n)"
if ($confirm -eq "y") {
  write-host "DEAD LIFT - START"
  write-host ""
  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/dead-lift/$(get-date -format 'yyyy-MM-dd_HHmmss')_dead-lift.txt" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT" "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"   "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"   "$DO_LOG" `

  $home_done = $True
  write-host "DEAD LIFT - DONE"
  write-host ""
  write-host ""
  write-host ""
}

write-host "ALL DONE!"
write-host "Remember to upload your logs via 'git commit...'."
write-host ""
write-host ""
write-host ""
write-host ""
write-host ""
