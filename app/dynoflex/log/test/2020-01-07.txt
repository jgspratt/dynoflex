Windows PowerShell
Copyright (C) Microsoft Corporation. All rights reserved.

PS C:\WINDOWS\system32> cd C:\Users\justin\Documents\git\dynoflex\app\dynoflex
PS C:\Users\justin\Documents\git\dynoflex\app\dynoflex> .\flex.ps1
START dynoflex.exe...

PULL DOWN - START

Begin Dynoflex...
COM Port Number: 3

Press Enter to connect to the Dynoflex.
Define comHubPorts... done!
Create the SysManager object...done!

FindComHubPorts - START
Found Teknic SC4-HUB
Friendly name: Teknic ClearPath 4-axis SC Hub (COM3)
Port number COM3
Found 1 SC Hubs (comHubPorts.size()=1)
FindComHubPorts - DONE

sys_manager->ComHubPort(0, 3) - START
sys_manager->ComHubPort() - DONE

sys_manager->PortsOpen() - START
sys_manager->PortsOpen() - DONE

IPort &this_port - START
IPort &this_port - DONE

COM Port[0]: port_state=5, node_count=1
   Node[0]: type=3
            userID: Dynoflex 001 - 20lbs load
        FW version: 1.7.6 5E67
          Serial #: 1220023
             Model: CPM-SCHP-N1433P-ELNA-1-7-D
Node    0 enabled

*** EXERCISE ***
Moves in this set: 8 (one rep is typically two moves)
Press Enter to begin this set.
4304.057456 ms estimated time.


Move 1 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 10.00 %
- delta:           -200000 counts
- log_this:        true
- time:            4.3s

Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.4) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.2) to exceed 10.0...
Waiting for actual torque (currently 5.3) to exceed 10.0...
Waiting for actual torque (currently 5.2) to exceed 10.0...
Waiting for actual torque (currently 5.6) to exceed 10.0...
Waiting for actual torque (currently 6.3) to exceed 10.0...
Waiting for actual torque (currently 6.5) to exceed 10.0...
Waiting for actual torque (currently 7.2) to exceed 10.0...
Waiting for actual torque (currently 8.0) to exceed 10.0...
Waiting for actual torque (currently 9.7) to exceed 10.0...
START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,11.9,3.9,19.7
-10000,10.5,2.5,12.7
-20000,10.5,2.5,12.6
-30000,8.7,0.7,3.5
-40000,8.0,0.0,0.1
-50000,8.2,0.2,0.8
-60000,8.4,0.4,2.0
-70000,7.5,-0.5,-2.5
-80000,8.0,0.0,0.1
-90000,8.2,0.2,1.1
-100000,8.0,0.0,0.1
-110000,7.9,-0.1,-0.5
-120000,7.9,-0.1,-0.7
-130000,8.1,0.1,0.4
-140000,7.2,-0.8,-3.8
-150000,7.9,-0.1,-0.4
-160000,7.8,-0.2,-1.1
-170000,8.0,0.0,0.1
-180000,7.9,-0.1,-0.5
-190000,7.6,-0.4,-2.2
-200000,6.6,-1.4,-7.1

Move Done
4304.057456 ms estimated time.


Move 2 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,5.3,5.3,26.4
10000,5.5,5.5,27.4
20000,5.2,5.2,25.8
30000,4.6,4.6,23.1
40000,5.3,5.3,26.4
50000,4.6,4.6,23.2
60000,5.2,5.2,26.1
70000,5.2,5.2,26.0
80000,4.9,4.9,24.3
90000,4.9,4.9,24.4
100000,5.2,5.2,26.0
110000,4.8,4.8,24.2
120000,4.5,4.5,22.4
130000,5.3,5.3,26.3
140000,4.4,4.4,21.9
150000,4.8,4.8,24.1
160000,4.6,4.6,23.0
170000,5.1,5.1,25.4
180000,5.5,5.5,27.7
190000,5.2,5.2,26.0
200000,5.0,5.0,25.1

Move Done
4304.057456 ms estimated time.


Move 3 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           -200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,6.5,-1.5,-7.5
-10000,7.2,-0.8,-4.1
-20000,7.7,-0.3,-1.7
-30000,7.2,-0.8,-4.2
-40000,7.2,-0.8,-4.0
-50000,7.7,-0.3,-1.7
-60000,7.8,-0.2,-1.2
-70000,6.9,-1.1,-5.7
-80000,7.8,-0.2,-1.2
-90000,7.5,-0.5,-2.5
-100000,7.7,-0.3,-1.5
-110000,7.5,-0.5,-2.5
-120000,7.7,-0.3,-1.4
-130000,8.0,-0.0,-0.2
-140000,7.8,-0.2,-0.8
-150000,8.2,0.2,0.8
-160000,8.0,0.0,0.1
-170000,7.9,-0.1,-0.4
-180000,8.0,0.0,0.1
-190000,7.6,-0.4,-2.0
-200000,6.6,-1.4,-6.8

Move Done
4304.057456 ms estimated time.


Move 4 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,5.2,5.2,26.2
10000,5.6,5.6,27.9
20000,5.2,5.2,25.9
30000,4.7,4.7,23.7
40000,5.1,5.1,25.5
50000,4.6,4.6,22.8
60000,5.3,5.3,26.3
70000,5.4,5.4,27.1
80000,4.5,4.5,22.3
90000,5.1,5.1,25.6
100000,5.2,5.2,25.8
110000,5.1,5.1,25.4
120000,4.3,4.3,21.6
130000,5.0,5.0,24.8
140000,5.2,5.2,26.1
150000,4.4,4.4,22.0
160000,4.7,4.7,23.4
170000,5.5,5.5,27.7
180000,5.3,5.3,26.3
190000,5.1,5.1,25.3
200000,5.0,5.0,24.9

Move Done
4304.057456 ms estimated time.


Move 5 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           -200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,6.5,-1.5,-7.4
-10000,7.5,-0.5,-2.3
-20000,8.0,-0.0,-0.2
-30000,7.6,-0.4,-2.2
-40000,7.3,-0.7,-3.4
-50000,7.7,-0.3,-1.4
-60000,7.7,-0.3,-1.6
-70000,7.3,-0.7,-3.4
-80000,7.3,-0.7,-3.3
-90000,7.4,-0.6,-3.0
-100000,7.5,-0.5,-2.4
-110000,7.5,-0.5,-2.5
-120000,7.3,-0.7,-3.5
-130000,7.9,-0.1,-0.5
-140000,7.6,-0.4,-2.2
-150000,7.9,-0.1,-0.6
-160000,8.1,0.1,0.7
-170000,7.9,-0.1,-0.7
-180000,7.9,-0.1,-0.5
-190000,7.6,-0.4,-2.0
-200000,6.8,-1.2,-6.0

Move Done
4304.057456 ms estimated time.


Move 6 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,5.5,5.5,27.3
10000,5.6,5.6,28.2
20000,5.2,5.2,25.9
30000,4.7,4.7,23.7
40000,4.9,4.9,24.4
50000,4.6,4.6,23.1
60000,5.3,5.3,26.7
70000,5.1,5.1,25.4
80000,4.7,4.7,23.6
90000,4.9,4.9,24.5
100000,5.2,5.2,25.9
110000,4.3,4.3,21.4
120000,4.4,4.4,22.0
130000,5.2,5.2,26.1
140000,4.4,4.4,22.0
150000,4.6,4.6,23.1
160000,5.2,5.2,26.1
170000,5.4,5.4,27.0
180000,5.5,5.5,27.4
190000,5.2,5.2,25.9
200000,5.0,5.0,25.1

Move Done
4304.057456 ms estimated time.


Move 7 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           -200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,6.4,-1.6,-7.8
-10000,6.8,-1.2,-5.9
-20000,7.5,-0.5,-2.3
-30000,7.3,-0.7,-3.3
-40000,7.2,-0.8,-4.0
-50000,7.7,-0.3,-1.4
-60000,7.7,-0.3,-1.7
-70000,7.1,-0.9,-4.5
-80000,7.4,-0.6,-3.0
-90000,7.5,-0.5,-2.6
-100000,7.5,-0.5,-2.4
-110000,7.6,-0.4,-1.9
-120000,7.4,-0.6,-2.9
-130000,8.0,-0.0,-0.1
-140000,7.8,-0.2,-1.0
-150000,8.0,0.0,0.2
-160000,8.1,0.1,0.6
-170000,7.9,-0.1,-0.7
-180000,7.9,-0.1,-0.3
-190000,7.3,-0.7,-3.4
-200000,6.7,-1.3,-6.5

Move Done
4304.057456 ms estimated time.


Move 8 (of 8):
- max_acc:         200 RPM/s
- max_torque:      100 %
- TrqGlobal:       99.996948
- max_vel:         100 RPM
- torque_to_start: 3.00 %
- delta:           200000 counts
- log_this:        true
- time:            4.3s

START MOVE
MOVE STARTED



count,torque,torque_compensated,lbs_compensated
0,5.4,5.4,27.2
10000,5.6,5.6,28.1
20000,5.2,5.2,26.2
30000,4.8,4.8,24.0
40000,4.9,4.9,24.7
50000,4.5,4.5,22.6
60000,5.3,5.3,26.7
70000,5.0,5.0,25.0
80000,4.8,4.8,23.8
90000,5.4,5.4,27.1
100000,5.2,5.2,26.0
110000,4.9,4.9,24.3
120000,4.1,4.1,20.5
130000,5.3,5.3,26.4
140000,4.6,4.6,23.1
150000,4.8,4.8,24.1
160000,4.6,4.6,23.2
170000,5.1,5.1,25.6
180000,5.3,5.3,26.5
190000,5.3,5.3,26.6
200000,5.1,5.1,25.4

Move Done

End Dynoflex!
Press Enter to complete set.

PULL DOWN - DONE

...DONE!
PS C:\Users\justin\Documents\git\dynoflex\app\dynoflex>