# Usage: See README.md

$COM_PORT                = 4  # See device manager
$RESOLUTION_COUNTS       = 10000

$ACC                     = 200  # Acceleration (RPM/s)
$TRQ                     = 100  # Torque (%)
$RPM                     = 120  # Max speed (RPM)

$DO_LOG                  = 1  # Yes, log
$NO_LOG                  = 0  # No, do not log
$LOG_DIR                 = 'log/jgs'

$PRE_TRQ_PCT             = 0.0  # Torque before a pre-move (usually zero)
$START_TRQ_PCT           = 4.0  # Torque required to start a move
$CONT_TRQ_PCT            = 2.0  # Torque required to continue a move

## Pull Down
$PULL_DOWN_COUNT         = 500000

## Shoulder Press
$SHOULDER_PRESS_PRE_POST = 100000
$SHOULDER_PRESS_COUNT    = 450000

## Dead Lift
$DEAD_LIFT_COUNT         = 400000

$pre_home_done = $False
$pre_home = 1

write-host @"




_ _ _ ____ _    ____ ____ _  _ ____    ___  ____ ____ _  _      ____ ____ _  _ _  _ ____ _  _ ___  ____ ____
| | | |___ |    |    |  | |\/| |___    |__] |__| |    |_/       |    |  | |\/| |\/| |__| |\ | |  \ |___ |__/
|_|_| |___ |___ |___ |__| |  | |___    |__] |  | |___ | \_ .    |___ |__| |  | |  | |  | | \| |__/ |___ |  \
                                                           '





"@
write-host ""
write-host ""
write-host ""
write-host ""
write-host "START dynoflex.exe..."
write-host ""


## PULL DOWN
############
$confirm = read-host "READY FOR ***PULL DOWN*** (WIDE, PALM OUT) (PEG 3)? (y/n)"
if ($confirm -eq "y") {
  write-host "PULL DOWN - START"
  write-host ""

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/pull-down_wide-palm-out/$(get-date -format 'yyyy-MM-dd_HHmmss')_pull-down_wide-palm-out.txt" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT" "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT"  "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT"  "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "PULL DOWN - DONE"
  write-host ""
  write-host ""
  write-host ""
}

## SHOULDER PRESS
##############
$confirm = read-host "READY FOR ***SHOULDER PRESS*** (PEG 1)? (y/n)"
if ($confirm -eq "y") {
  write-host "SHOULDER PRESS - START"
  write-host ""

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/shoulder-press/$(get-date -format 'yyyy-MM-dd_HHmmss')_shoulder-press.txt" `
    "$ACC" "5"    "$RPM" "$PRE_TRQ_PCT"   "$SHOULDER_PRESS_PRE_POST"   "$NO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT" "$SHOULDER_PRESS_COUNT"     "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$SHOULDER_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$SHOULDER_PRESS_COUNT"    "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "SHOULDER PRESS - DONE"
  write-host ""
  write-host ""
  write-host ""
}


## DEAD LIFT
############
$confirm = read-host "READY FOR ***DEAD LIFT*** (SEAT REMOVED)? (y/n)"
if ($confirm -eq "y") {
  write-host "DEAD LIFT - START"

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/dead-lift/$(get-date -format 'yyyy-MM-dd_HHmmss')_dead-lift.txt" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT"  "$DEAD_LIFT_COUNT"          "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"   "$DEAD_LIFT_COUNT"         "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "-$DEAD_LIFT_COUNT"         "$DO_LOG" `

  $home_done = $True
  write-host "DEAD LIFT - DONE"
  write-host ""
  write-host ""
  write-host ""
}

write-host "ALL DONE!"
write-host "Remember to upload your logs via 'git commit...'."
write-host ""
write-host ""
write-host ""
write-host ""
write-host ""
