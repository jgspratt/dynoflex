# Usage: See README.md

$COM_PORT             = 4  # See device manager
$RESOLUTION_COUNTS    = 10000

$ACC                  = 200  # Acceleration (RPM/s)
$TRQ                  = 100  # Torque (%)
$RPM                  = 100  # Max speed (RPM)

$DO_LOG               = 1  # Yes, log
$NO_LOG               = 0  # No, do not log
$LOG_DIR              = 'log/gs'

$PRE_TRQ_PCT          = 0.0  # Torque before a pre-move (usually zero)
$START_TRQ_PCT        = 3.0  # Torque required to start a move
$CONT_TRQ_PCT         = 2.0  # Torque required to continue a move

# Pull Down
$PULL_DOWN_COUNT      = 580000

# Chest Press
$CHEST_PRES_PRE_POST  = 150000
$CHEST_PRESS_COUNT    = 363000

## Dead Lift
$DEAD_LIFT_COUNT      = 300000


write-host @"




  ,          _   _
 /|   |     | | | |          () |                    o
  |___|  _  | | | |  __      /\/| _   _  _    _   ,      ,
  |   |\|/  |/  |/  /  \_   /   ||/  / |/ |  |/  / \_|  / \_
  |   |/|__/|__/|__/\__/o  /(__/ |__/  |  |_/|__/ \/ |_/ \/
                        /




"@
write-host "START dynoflex.exe..."
write-host ""


## PULL DOWN
############
$confirm = read-host "READY FOR ***PULL DOWN***? (y/n)"
if ($confirm -eq "y") {
  write-host "PULL DOWN - START"
  write-host ""
  if (-not $home_done) {$pre_home = 1} else {$pre_home = 0}

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/pull-down/$(get-date -format 'yyyy-MM-dd_HHmmss')_pull-down.txt" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT"  "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$PULL_DOWN_COUNT" "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$PULL_DOWN_COUNT" "$DO_LOG" `

  $home_done = $True
  write-host ""
  write-host "PULL DOWN - DONE"
  write-host ""
  write-host ""
  write-host ""
}

## CHEST PRESS
##############
$confirm = read-host "READY FOR ***CHEST PRESS***? (y/n)"
if ($confirm -eq "y") {
  write-host "CHEST PRESS - START"
  write-host ""
  if (-not $home_done) {$pre_home = 1} else {$pre_home = 0}

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/chest-press/$(get-date -format 'yyyy-MM-dd_HHmmss')_chest-press.txt" `
    "$ACC" "5"    "$RPM" "$PRE_TRQ_PCT" "$CHEST_PRES_PRE_POST"   "$NO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT"  "$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$CHEST_PRESS_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$PRE_TRQ_PCT"  "-$CHEST_PRES_PRE_POST" "$NO_LOG" `

  $home_done = $True
  write-host ""
  write-host "CHEST PRESS - DONE"
  write-host ""
  write-host ""
  write-host ""
}

## DEAD LIFT
############
$confirm = read-host "READY FOR ***DEAD LIFT***? (y/n)"
if ($confirm -eq "y") {
  write-host "DEAD LIFT - START"
  if (-not $home_done) {$pre_home = 1} else {$pre_home = 0}

  .\Win32\Release\dynoflex.exe `
    "$COM_PORT" `
    "$RESOLUTION_COUNTS" `
    "$pre_home" `
    "${LOG_DIR}/dead-lift/$(get-date -format 'yyyy-MM-dd_HHmmss')_dead-lift.txt" `
    "$ACC" "$TRQ" "$RPM" "$START_TRQ_PCT"  "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT"  "$DEAD_LIFT_COUNT"    "$DO_LOG" `
    "$ACC" "$TRQ" "$RPM" "$CONT_TRQ_PCT" "-$DEAD_LIFT_COUNT"    "$DO_LOG" `

  $home_done = $True
  write-host "DEAD LIFT - DONE"
  write-host ""
  write-host ""
  write-host ""
}

write-host "ALL DONE!"
write-host "Remember to upload your logs via 'git commit...'."
write-host ""
write-host ""
write-host ""
write-host ""
write-host ""
