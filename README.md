# Welcome to The DynoFlex project

Check out the [DynoFlex YouTube playlist](https://www.youtube.com/playlist?list=PLwjMQS0K-8cQiG_PMJ7GG23SJUcnesbrm).


## Warnings

By accessing this repo, you agree to the following statements:

1. This repo and linked material is provided for informational purposes only
2. Serious personal harm (including, but not limited to, serious injury, death, financial loss, and property damage) could come from implementing these designs.
3. No medical or fitness or exercise advice is rendered.  All statements related to health and fitness are personal opinions only.
4. Everything related to DynoFlex is provided without warranty or guarantee.
5. You must operate strictly within all laws which apply to you and your jurisdiction.
6. You must include these warnings with all distributions of this repo and any information related to DynoFlex.



## What is DynoFlex?

It is an open-source exercise machine based on powerful servo motors.



## Parts List

Below are the exact parts I used for build #1.  It is possible to substitute other parts if you make appropriate adjustments: for example, you can use a different gearbox, but make sure your shaft diameters line up with your motor and your pulley assembly.

Excuse the mix of metric and imperial units: the Bowflex is an American product, but the gearbox is not.

Part list format below is as follows:

```
<heading_number>. <heading>
<list_number>. <quantity> @ <part name> (<comment>)
```

Part IDs referenced elsewhere use this list in the following way: `<heading_number>.<list_number>`, so part `1.1` would be the first part under the first heading.

### 1. Official Bowflex parts

1. 1 @ [BowFlex Xtreme 2 SE](https://www.bowflex.com/home-gyms/x2se/100334.html) (I used the Pre-2013 model specifically [manual](https://download.nautilus.com/supportdocs/OM/Bowflex/BFX_Xtreme2_OM_RevB_web.pdf).  This is part `1.1`)

### 2. Teknic Parts

1. 1 @ [The motor itself: CPM-SCHP-N1433A-ELNA_Fan](https://www.teknic.com/model-info/CPM-SCHP-N1433A-ELNA_Fan/)
    1. Personally, I'd actually go with [CPM-SCHP-D1003P-ELNA](https://www.teknic.com/model-info/CPM-SCHP-D1003P-ELNA/) if I had to build another one, but then I would be getting a different gearbox than the one below and possibly a different belt pulley drum and I would need a different reducer mount flange.
2. 1 @ [Communication Hub for ClearPath-SC Series - SC4-HUB](https://www.teknic.com/SC4-HUB/)
3. 1 @ [24 Volt Power, 2-Pin Molex Cable - CPM-CABLE-M2P2P-120](https://www.teknic.com/cpm-cable-m2p2p-120/)
    1. Instead, you could make your own (as I did) with [these Molex Connectors](https://www.amazon.com/gp/product/B074M1RZHX) and this [18 AWG speaker wire](https://www.amazon.com/gp/product/B017SDDDL4))
4. 1 @ [I/O Cable, 4-Pin Molex - CPM-CABLE-M4P4P-120](https://www.teknic.com/cpm-cable-m4p4p-120/)


### 3. Reducer Mount

1. 2 @ [gear-reducer-flange-base-plate_2-at-0.25-inch.DXF](/hardware/sheet_metal/gear-reducer-flange_1-at-0.25-inch.DXF)
2. 1 @ [gear-reducer-flange_1-at-0.25-inch.DXF](/hardware/sheet_metal/gear-reducer-flange_1-at-0.25-inch.DXF)
3. 2 @ [gear-reducer-flange-support-triangle-acute_2-at-0.25-inch.DXF](/hardware/sheet_metal/gear-reducer-flange-support-triangle-acute_2-at-0.25-inch.DXF)
4. 2 @ [gear-reducer-flange-support-triangle-obtuse_2-at-0.25-inch-thick.DXF](/hardware/sheet_metal/gear-reducer-flange-support-triangle-obtuse_2-at-0.25-inch-thick.DXF)


### 4. Winding pulley drum

1. 1 @ [Timing Pulleys L Type - ATPT26L100-A-N32-NFC](https://us.misumi-ec.com/vona2/detail/110300405670/?HissuCode=ATPT26L100-A-N32-NFC) (belt pulley drum - note `NFC` "No Flange" option)
2. 2 @ [pulley-drum-flange_2-at-14-gauge.DXF](/hardware/sheet_metal/pulley-drum-flange_2-at-14-gauge.DXF)
3. 1 @ [pulley-drum-flange-bolt-holder_1-at-14-gauge.DXF](/hardware/sheet_metal/pulley-drum-flange-bolt-holder_1-at-14-gauge.DXF)


### 5. Upper Cable Reverser

1. 2 @ [cable-pulley-bracket_2-at-0.25-inch.DXF](/hardware/sheet_metal/cable-pulley-bracket_2-at-0.25-inch.DXF)


### 5. Other Electronic parts

1. 1 @ [24V @ 2A power supply](https://www.amazon.com/gp/product/B01B1PRE42)
2. 1 @ [Emergency Stop Button 22mm](https://www.amazon.com/gp/product/B019DSZWPC)


### 5. Gearbox parts

1. 1 @ NewStart 10:1 planetary gear reducer `AB115L1-10-P2-S2-T-22.23-54-114.3-149.23-10` (you should probably simply contact [NewStart](http://www.newstartmotion.com/indexen.html) as it is a semi-custom part)
    1. It is important to consider your nominal output torque.  This reducer is rated for 230 N-m (170 ft-lb) of torque.


### 6. Nuts and Bolts

1. 3 @ 7 inch lengths of [standard 1/2 inch (1/2-13 UNC) threaded rod](https://www.homedepot.com/p/Everbilt-1-2-in-x-36-in-Zinc-Threaded-Rod-802257/204274019) (for Upper Cable Reverser)
2. 12 @ [standard 1/2 inch (1/2-13 UNC) nuts](https://www.homedepot.com/p/Everbilt-1-2-in-13-tpi-Zinc-Plated-Hex-Nut-50-Piece-per-Box-804710/204274094) (for Upper Cable Reverser)
3. 8 @ [1/2 inch washer](https://www.homedepot.com/p/1-2-in-Zinc-Plated-Flat-Washer-25-Pack-802334/204276390) (for Upper Cable Reverser)
4. 4 @ [3/8 inch (3/8-16 UNC) 1 inch long hex bolts rated](https://www.mscdirect.com/product/details/04201133) (motor bolts)
5. 4 @ m8 x 1.25 bolts 35mm long (gear reducer to flange connection)
6. 4 @ m8 nuts (gear reducer to flange connection)
7. 4 @ m8 locking washers (gear reducer to flange connection)
8. 8 @ m8 regular washers (gear reducer to flange connection)
9. 4 @ 5/16 (5/16-18 UNC) 1.5 inch bolts (reducer mount base plate to Bowflex connection)
10. 4 @ 5/16 (5/16-18 UNC) nuts (reducer mount base plate to Bowflex connection)
10. 8 @ 5/16 washers (reducer mount base plate to Bowflex connection)


### 7. Belt and connectors

1. 30 feet @ [1 Inch Flat Nylon Webbing](https://www.strapworks.com/Flat_Nylon_Webbing_p/fnw1.htm) (main belt)
2. 1 @ [3/8 in. Stainless Steel Quick Link](https://www.homedepot.com/p/Everbilt-3-8-in-Stainless-Steel-Quick-Link-42794/205883033) (belt coupler)
3. (optional) 2 @ [5/16 in. Stainless Steel Quick Link](https://www.homedepot.com/p/Everbilt-5-16-in-Stainless-Steel-Quick-Link-43404/205887496) [Amazon link](https://www.amazon.com/Pack-Steel-Snap-Spring-Hooks/dp/B073LN65C8) (same as original Bowflex quick connects)



## FAQ

### How do I use it?

Right now (2019-05-04), the only thing that is building is the `app/examples/1-HelloWorld/HelloWorld.sln` Visual Studio project.  It doesn't really do anything besides build and move the motor back and forth a bit.

I installed Visual Studio Community 2017 (which requires a Microsoft Account, but no actual payment).

This project has a bunch of C++ links and stuff.  I will do my best to document here how I set this up.


### Q: What if I want to adjust the gearing?

A: TODO


### Q: What software did you use to design the sheet metal parts?

A: I used [eMachineShop CAD](https://www.emachineshop.com/free-download/).  A friend who created the [pulley-drum-flange_2-at-14-gauge.DXF](/hardware/sheet_metal/pulley-drum-flange_2-at-14-gauge.DXF) used [SolidWorks](https://www.solidworks.com/).

